package com.camilo.main;

import com.camilo.models.Pessoa;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Concluído: Aula 2 - Ambiente de Desenvolvimento e Sintaxe U1 (GEE12007)
 * Atividade 4: Escreva um programa em Java que leia a idade e a altura de 10 pessoas.
 * Calcule e informe a média das alturas das pessoas com mais de 50 anos. Para isso, use for.
 */
class Atividade4IdadeMediaPessoas {

	private static final int IDADE_MINIMA_CALCULO_ALTURA = 50;
	private static final int TOTAL_DA_QUANTIDADE_DE_PESSOAS = 10;

	private static final Logger logger = Logger.getLogger(Atividade4IdadeMediaPessoas.class.getCanonicalName());


	private Atividade4IdadeMediaPessoas() throws IllegalAccessException {
		throw new IllegalAccessException("Classe não pode ser instanciada");
	}

	public static void main(String[] args) {
		Integer contadorDaQuantidadeDoLoop = 0;
		final Pessoa[] pessoas = new Pessoa[TOTAL_DA_QUANTIDADE_DE_PESSOAS];
		while (contadorDaQuantidadeDoLoop < TOTAL_DA_QUANTIDADE_DE_PESSOAS) {
			final ResultadoDaGeracao resultadoDaGeracao = gerarPessoasParaInserirNoArray(contadorDaQuantidadeDoLoop);
			pessoas[contadorDaQuantidadeDoLoop] = resultadoDaGeracao.getPessoa();
			contadorDaQuantidadeDoLoop = resultadoDaGeracao.getContador();
		}

		final Double mediaDeAltura = calcularAlturaMediaPessoasDe50Anos(pessoas);
		if (mediaDeAltura > 0d) {
			System.out.println(String.format("A media de altura das pessoas com mais de 50 anos é dê %s metros", mediaDeAltura));
		} else {
			System.out.println("Não possui pessoas com mais de 50 anos para calcular a media");
		}

	}


	private static ResultadoDaGeracao gerarPessoasParaInserirNoArray(Integer contadorDaQuantidadeDoLoop) {
		final ResultadoDaGeracao resultadoDaGeracao = new ResultadoDaGeracao();
		final InputStreamReader inputStreamReader = new InputStreamReader(System.in);
		final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		final int contadorParaExibicao = contadorDaQuantidadeDoLoop + 1;
		try {
			System.out.println(String.format("================== Inicinado o cadastro da Pessoa %s ==================", contadorParaExibicao));
			Pessoa pessoa = new Pessoa();
			System.out.println(String.format("Informe a idade da pessoa %d, exemplo 50", contadorParaExibicao));
			pessoa.addIdadeAsString(bufferedReader.readLine());

			System.out.println(String.format("Informe a altura da pessoa %d, exemplo 1.9", contadorParaExibicao));
			pessoa.addAlturaAsString(bufferedReader.readLine());
			contadorDaQuantidadeDoLoop++;
			System.out.println("================ Finalizando o cadastro  ================");
			return resultadoDaGeracao.addContador(contadorDaQuantidadeDoLoop).addPessoa(pessoa);

		} catch (Exception e) {
			logger.log(Level.WARNING, e.getMessage());
			System.out.println("================== ERRO ==================");
			System.out.println(String.format("Reiniciando a inserção de dados para a Pessoa %s", contadorParaExibicao));
		}
		return resultadoDaGeracao.addContador(contadorDaQuantidadeDoLoop);
	}


	static Double calcularAlturaMediaPessoasDe50Anos(final Pessoa[] pessoas) {

		if (pessoas.length != TOTAL_DA_QUANTIDADE_DE_PESSOAS) {
			throw new IllegalArgumentException(String.format("Deve Possui exatamento %s", TOTAL_DA_QUANTIDADE_DE_PESSOAS));
		}

		final Counter counter = new Counter();
		for (final Pessoa pessoa : pessoas) {
			if (pessoa.getIdade() >= IDADE_MINIMA_CALCULO_ALTURA) counter.somarAlturaPessoa(pessoa.getAltura());
		}
		return counter.calcularAlturaMedia();
	}

	static class Counter {
		private BigDecimal totalAlturaPessoas = new BigDecimal(0);
		private int quantidadeTotalDePessoas = 0;


		Counter somarAlturaPessoa(final double altura) {
			totalAlturaPessoas = totalAlturaPessoas.add(BigDecimal.valueOf(altura));
			return somarQuantidadePessoa();
		}

		Counter somarQuantidadePessoa() {
			quantidadeTotalDePessoas++;
			return this;
		}

		Double calcularAlturaMedia() {
			final int SCALE = 2;
			final int VAZIO = 0;
			if (quantidadeTotalDePessoas == VAZIO) return (double) VAZIO;
			return totalAlturaPessoas.divide(BigDecimal.valueOf(quantidadeTotalDePessoas), SCALE, BigDecimal.ROUND_HALF_UP).doubleValue();
		}

	}

	static class ResultadoDaGeracao {
		private Integer contador;
		private Pessoa pessoa;

		ResultadoDaGeracao() {

		}

		Integer getContador() {
			return contador;
		}

		Pessoa getPessoa() {
			return pessoa;
		}

		ResultadoDaGeracao addContador(final Integer contador) {
			this.contador = contador;
			return this;
		}

		ResultadoDaGeracao addPessoa(final Pessoa pessoa) {
			this.pessoa = pessoa;
			return this;
		}
	}

}

