package com.camilo.models;

import java.util.Objects;

public class Pessoa {
	private long idade;
	private float altura;

	public Pessoa() {
	}

	public Pessoa(long idade, float altura) {
		this.idade = idade;
		this.altura = altura;
	}

	public long getIdade() {
		return idade;
	}

	public float getAltura() {
		return altura;
	}

	public Pessoa addIdadeAsString(final String idade) {
		try {
			this.idade = Integer.parseInt(idade);
		} catch (Exception e) {
			throw new IllegalArgumentException(String.format("Idade %s está incorreta, use o valor inteiro exemplo: 10 ,Tente Novamente", idade));
		}
		return this;
	}

	public Pessoa addAlturaAsString(final String altura) {
		try {
			this.altura = Float.parseFloat(altura);
		} catch (Exception e) {
			throw new IllegalArgumentException(String.format("Valor %s é incorreto para a altura, dever o valor no formato 0.00, Tente Novamente", altura));
		}

		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Pessoa pessoa = (Pessoa) o;
		return idade == pessoa.idade &&
				Float.compare(pessoa.altura, altura) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idade, altura);
	}
}
