package com.camilo.main;

import com.camilo.models.Pessoa;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Atividade4IdadeMediaPessoasTest {


	@Test
	public void deveCalcularAlturaMediaDasPessoasMaiorsDe50Anos() {
		Pessoa[] pessoas = new Pessoa[]{
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f)
		};
		final Double mediaAlturaPessoas = Atividade4IdadeMediaPessoas.calcularAlturaMediaPessoasDe50Anos(pessoas);
		assertEquals("Deve possuir altura media de 2.00 de altura para pessoas com mais de 50 anos", 2.0d, mediaAlturaPessoas, 0.0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void deveLancarExcpectionQuandoNaoForExatamente10Pessoas() {
		Atividade4IdadeMediaPessoas.calcularAlturaMediaPessoasDe50Anos(new Pessoa[]{});
	}


	@Test
	public void deveCalcularAlturaMediaCom5PessoasDe50Anos() {
		Pessoa[] pessoas = new Pessoa[]{
				new Pessoa(49, 2.00f),
				new Pessoa(49, 2.00f),
				new Pessoa(49, 2.00f),
				new Pessoa(49, 2.00f),
				new Pessoa(49, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f),
				new Pessoa(50, 2.00f)
		};
		final Double mediaAlturaPessoas = Atividade4IdadeMediaPessoas.calcularAlturaMediaPessoasDe50Anos(pessoas);
		assertEquals("Deve possuir altura media de 2.00 de altura para pessoas com mais de 50 anos com a metade do array preenchido", 2.0d, mediaAlturaPessoas, 0.0);
	}
}