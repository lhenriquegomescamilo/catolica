### Projeto para poder responder os exerício da católica

##### Passos para reprodução
1) É necessário ter o maven e o jdk 8 instalado na máquina
     * Instalação do java no windows: https://docs.oracle.com/javase/8/docs/technotes/guides/install/windows_jdk_install.html
     * Instalação do java no linux: https://www.javahelps.com/2015/03/install-oracle-jdk-in-ubuntu.html
     * Instalação do maven: https://maven.apache.org/install.html
2) Faça downlaod do projeto
3) Pelo terminal navegue até a pasta do projeto
4) Executar o comando `mvn install`
5) Éxecutar o comando `mvn package`
6) Após rodar os comando acima, execute a aplicação pelo comando : `java -jar ./target/catolica-1.0-SNAPSHOT.jar`


